import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
RAW_DATA_DIR_100k = os.path.join(ROOT_DIR, 'data/raw/ml-100k')
PROCESSED_DATA_DIR_100k = os.path.join(ROOT_DIR, 'data/processed/ml-100k')
RAW_DATA_DIR_20m = os.path.join(ROOT_DIR, 'data/raw/ml-20m')
PROCESSED_DATA_DIR_20m = os.path.join(ROOT_DIR, 'data/processed/ml-20m')


