import pandas as pd
import numpy as np
import math
from math import sqrt
import sys
from sklearn.metrics import mean_squared_error
sys.path.append('../../')
from src.preparation.data import Data
import warnings
from sklearn.preprocessing import scale


class ContentBased:
    def __init__(self):
        self.similarity = []
        # Attribute to store the training data, in our
        # case we want a normalised dataset to be reusable
        self.train_data = pd.Series({})
        self.test_data = pd.Series({})

    def fit(self, training):
        # Get movie genre array
        movies = pd.DataFrame(Data.get_movie())
        movies = movies.iloc[:, np.r_[0, 5:24]]
        genres = movies.iloc[:, 1:24]

        self.train_data = training

        # FINDING WHICH MOVIE IN TEST SET BUT NOT IN TRAIN
        # Setting an array of all movie id in train
        # temp = training.iloc[:, 1]
        # temp_array = temp.values
        # movie_id = []
        # for index in range(len(temp)):
        #     if temp_array[index] in movie_id:
        #         continue
        #     else:
        #         movie_id.append(temp_array[index])
        #
        # # Setting an array of all movie_id from 1 to 1683
        # all_movie = []
        # for index in range(1, 1683):
        #     all_movie.append(index)
        #
        # all_movie = pd.DataFrame({'movie_id': list(all_movie)})
        # movie_id = pd.DataFrame({'movie_id': list(movie_id)})
        #
        # # Merging training set with genres boolean columns
        # training_movie = pd.merge(movie_id, movies, on='movie_id')
        #
        # # movie_id 1653 and 1582 is not in train so set the genre into 0s
        # row_1653 = pd.DataFrame(
        #     {"movie_id": [1653], "unknown": [0], "Action": [0], "Adventure": [0], "Animation": [0], "Children's": [0],
        #      "Comedy": [0], "Crime": [0], "Documentary": [0], "Drama": [0], "Fantasy": [0], "Film-Noir": [0],
        #      "Horror": [0], "Musical": [0], "Mystery": [0], "Romance": [0], "Sci-Fi": [0], "Thriller": [0], "War": [0],
        #      "Western": [0]})
        # row_1582 = pd.DataFrame(
        #     {"movie_id": [1582], "unknown": [0], "Action": [0], "Adventure": [0], "Animation": [0], "Children's": [0],
        #      "Comedy": [0], "Crime": [0], "Documentary": [0], "Drama": [0], "Fantasy": [0], "Film-Noir": [0],
        #      "Horror": [0], "Musical": [0], "Mystery": [0], "Romance": [0], "Sci-Fi": [0], "Thriller": [0], "War": [0],
        #      "Western": [0]})
        #
        # training_movie.loc[1680] = row_1653.loc[0]
        # training_movie.loc[1681] = row_1582.loc[0]
        # training_movie = training_movie.sort_values('movie_id', ascending=True)

        # Getting the genres only
        genres_array = genres.values

        movie_id_array = movies.values
        similarity = []
        movie = []
        movie_target = []
        # Calculating cosine similarity between each movie
        for index in range(len(genres)):
            norm_a = np.linalg.norm(genres_array[index])
            for i in range(len(genres)):
                dot_product = np.dot(genres_array[index], genres_array[i])
                norm_b = np.linalg.norm(genres_array[i])
                similarity.append(dot_product / (norm_a * norm_b))
                movie.append(movie_id_array[index][0])
                movie_target.append(movie_id_array[i][0])

        # Set a new dataframe which has the format of movie_id, movie_target, and cosine_similarity
        movie_similarity = pd.DataFrame(
            {'movie_id': list(movie), 'movie_target': list(movie_target), 'cosine_similarity': list(similarity)},
            columns=['movie_id', 'movie_target', 'cosine_similarity'])

        movie_similarity = movie_similarity.fillna(0)
        # Create the pivot table of the similarity
        movie_similarity = pd.pivot_table(movie_similarity, values='cosine_similarity', index='movie_id',
                                          columns='movie_target')
        movie_similarity = pd.DataFrame(movie_similarity.fillna(0), index=movie_similarity.index,
                                        columns=movie_similarity.columns)

        self.similarity = movie_similarity

        return self

    @staticmethod
    def get_top_n_similar_movies(similarity_matrix, training_set, target_user, target_movie, N=3):
        # only get movies where a certain user has rated. i.e. all movies that user id 5 has rated.
        # This is like how lecture 5 works.
        rating_from_user = training_set[training_set['user_id'] == target_user]

        # get all similarity value from this movie to all other movies that has been rated by the same user in the train set.
        similar_users = similarity_matrix[target_movie].filter(items=rating_from_user['movie_id'].values)

        # Get the top n users.
        # get start from 1 because the first index "0" is always the target_movie itself.
        top_n_similar_users = similar_users.sort_values(ascending=False).iloc[1:(1 + N)]

        return top_n_similar_users

    def predict(self, test):
        self.test_data = test

        true_rating = self.train_data['rating']

        # Average rating used if no similar movies at all.
        # TODO: Can use the average rating for the movie instead?
        average_rating = round(true_rating.mean(), 3)
        predicted_rating = []

        # iterate through all the ratings data in the test set.
        for index, row in test.iterrows():
            this_movie_id = row['movie_id']
            this_user_id = row['user_id']
            try:
                cumulative_rating = 0
                cumulative_similarity = 0
                # Similarity value comes from the training set. (self.similarity)
                similar_movies = self.get_top_n_similar_movies(
                    self.similarity, self.train_data, this_user_id, this_movie_id)

                # Iterate through all similar movies
                for movie, similarity_value in similar_movies.iteritems():
                    user = self.train_data['user_id'] == this_user_id
                    movie = self.train_data['movie_id'] == movie
                    # Get the specific rating (row) that a user give to the similar movie
                    to_predict = self.train_data[user & movie]
                    cumulative_rating += round(
                        to_predict['rating'].values[0] * similarity_value, 3)
                    cumulative_similarity += round(similarity_value, 3)
                if cumulative_rating != 0 and cumulative_similarity != 0:
                    predicted_rating.append(round(cumulative_rating / cumulative_similarity, 3))
                else:
                    predicted_rating.append(average_rating)
            except KeyError:
                    predicted_rating.append(average_rating)

        return predicted_rating

