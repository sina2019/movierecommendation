from __future__ import division
import pandas as pd
import numpy as np
import time
import math
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import scale

import sys

sys.path.append('../../')
from src.preparation.utility import normalise_user_user_data


class BaselineUserCFModel:
    def __init__(self):
        self.similarity = []
        self.averages_by_id = pd.Series([])
        self.train_data = pd.Series({})
        self.test_data = pd.Series({})
        self.predicted = pd.Series({})

    def fit(self, training):
        # Get cosine similarity
        # user_movie_rating = pd.DataFrame({'user': training['user_id'],
        #                               'movie': training['movie_id'],
        #                               'rating': training['rating']})
        # user_movie_rating = user_movie_rating.pivot_table(index='user', columns='movie', values='rating').fillna(0)

        # similarity = pd.DataFrame(cosine_similarity(
        #     scale(user_movie_rating.fillna(0))),
        #     index=user_movie_rating.index,
        #     columns=user_movie_rating.index
        # )

        # Get Normalized Cosine Similarity ##############################################
        normalised_training = normalise_user_user_data(training)

        self.averages_by_id = (
            normalised_training.groupby('user_id')['rating']
            .mean()
            .rename('average_rating')
        )

        self.train_data = normalised_training

        # Pivot the table into user as row, movie as column, rating as the values that each user row has for the movie.
        normalised_user_movie_rating = normalised_training.pivot_table(index='user_id', columns='movie_id',
                                                                       values='rating').fillna(0)
        # Get the cosine similarity (user-user).
        normalised_user_similarity = pd.DataFrame(cosine_similarity(
            scale(normalised_user_movie_rating.fillna(0))),
            index=normalised_user_movie_rating.index,
            columns=normalised_user_movie_rating.index
        )

        self.similarity = normalised_user_similarity
        return self

    @staticmethod
    def get_top_n_similar_users(similarity_matrix, training_set, target_user, target_movie, n=3):
        # get start from 1 because the first index "0" is always the target_user itself.

        # only get users that has rate this movie in the training set.
        rating_to_this_movie = training_set[training_set['movie_id'] == target_movie]

        # get all similarity value from this user to all the users that has rated this movie in the train set.
        similar_users = similarity_matrix[target_user].filter(items=rating_to_this_movie['user_id'].values)

        # Get the top n users.
        top_n_similar_users = similar_users.sort_values(ascending=False).iloc[1:(1+n)]

        return top_n_similar_users

    def predict(self, test):
        normalised_test = normalise_user_user_data(test)
        self.test_data = normalised_test

        true_rating = self.train_data['rating']

        # Average rating used if no similar users at all.
        # TODO: Can use the average rating from the user instead?
        average_rating = round(true_rating.mean(), 3)
        predicted_rating = [0] * len(self.test_data['rating'])

        # iterate through all the ratings data in the test set.
        for index, row in normalised_test.iterrows():
            this_user_id = row['user_id']
            this_movie_id = row['movie_id']

            # Store cumulative rating and cumulative similarity to find the
            # final rating prediction for top n similar users
            cumulative_rating = 0
            cumulative_similarity = 0

            similar_users = self.get_top_n_similar_users(
                self.similarity, self.train_data, this_user_id, this_movie_id, 3)

            # Iterate through all similar users
            for user, similarity_value in similar_users.iteritems():
                similarity_value = round(similarity_value, 3)
                user = self.train_data['user_id'] == user
                movie = self.train_data['movie_id'] == this_movie_id
                # Get the specific rating (row) from the similar user to the movie.
                to_predict = self.train_data[user & movie]
                if not to_predict.empty:
                    cumulative_rating += round(
                        to_predict['rating'].values[0] * similarity_value, 3)
                    cumulative_similarity += round(similarity_value, 3)
            if cumulative_rating != 0 and cumulative_similarity != 0:
                cumulative_rating = round(cumulative_rating, 2)
                cumulative_similarity = round(cumulative_similarity, 3)
                predicted_rating[index] = round(float(cumulative_rating / cumulative_similarity), 2)
            else:
                try:
                    predicted_rating[index] = self.averages_by_id[this_user_id]
                except KeyError:
                    predicted_rating[index] = average_rating

            if math.isnan(predicted_rating[index]) or np.isinf(predicted_rating[index]):
                try:
                    predicted_rating[index] = self.averages_by_id[this_user_id]
                except KeyError:
                    predicted_rating[index] = average_rating

        return predicted_rating


## TESTING PURPOSE.
# from src.preparation.data import Data
# from src.modeling.Models import Models

# datasets = Data.get_rating(filtered=True)


# models = {}
# models.update({'User CF': BaselineUserCFModel()})

# models = Models(models)

# print(models.get_rmse(datasets))
# import time

# start = time.time()
# training_a, testing_a = Data.get_datasets_a()
# data = BaselineUserCFModel().fit(training_a).predict(testing_a)
# end = time.time()
# print(end - start)
#
# # BaselineUserCFModel().get_recommendation(1, testing_a, data)
#
# from math import sqrt
# from sklearn.metrics import mean_squared_error
# #
# print(sqrt(mean_squared_error(normalise_user_user_data(testing_a)['rating'].values, data)))


# a = BaselineUserCFModel().fit(training_a)
# similar_users = a.get_top_n_similar_users(
#                 a.similarity, normalise_user_user_data(training_a), 1, 1, 3)
# print(similar_users)
