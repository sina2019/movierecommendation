import pandas as pd


class AverageByIdModel:
    def __init__(self, column_id):
        self.column_id = column_id
        self.rating_column = 'rating'
        # Create an empty series, populated once we fit it.
        self.averages_by_id = pd.Series([])
        self.overall_average = 0

    def fit(self, training):
        training = training[[self.column_id, self.rating_column]].copy()
        # Rename column.
        training.columns = ['id', 'rating']
        # Get average of all ratings for the same id ('movie_id' or 'user_id')
        self.averages_by_id = (
            training.groupby('id')['rating']
            .mean()
            .rename('average_rating')
        )
        # Overall average is used if the user id given in test set is non existence in the training set.
        self.overall_average = training['rating'].mean()

        return self

    def predict(self, test):
        test = test[[self.column_id, self.rating_column]].copy()
        test.columns = ['id', 'rating']

        # Join the test table and averages by id table if they have the same id.
        test = test.join(self.averages_by_id, on='id')

        # if the user id given in test set is non existence in the training set, then fill
        # the "NULL" with the average_rating.
        test['average_rating'].fillna(self.overall_average, inplace=True)

        # Average rating for all the id (user_id or movie_id) will be returned
        return test['average_rating'].values





