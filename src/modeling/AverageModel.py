import numpy as np


class AverageModel:

    def __init__(self):
        self.mean_ratings = 0

    def fit(self, train):
        # Get the mean of all ratings value
        self.mean_ratings = train.iloc[:, 2].mean()
        return self

    def predict(self, test):
        # Return an array with the same length of the test with the value of mean_ratings.
        return np.ones(len(test)) * self.mean_ratings


## TESTING PURPOSE.

#
# from src.preparation.data import Data
# from src.modeling.Models import Models
#
# datasets = Data.get_rating(bigger=True)
#
# models = {}
# models.update({'Average': AverageModel()})
#
# models = Models(models)
#
# rmses, recalls = (models.evaluate(datasets))


# start = time.time()
# training_a, testing_a = Data.get_datasets_a()
# data = BaselineUserCFModel().fit(training_a).predict(testing_a)
# end = time.time()
# print(end - start)