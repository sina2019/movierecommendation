from surprise import Dataset
from surprise.model_selection import train_test_split, cross_validate
from collections import defaultdict

class Surprise:

    def __init__(self, algo, dataset, reader):
        self.algo = algo
        self.data = Dataset.load_from_df(dataset, reader)
        self.train, self.test = train_test_split(self.data, test_size=0.2)

    @staticmethod
    def read(data, reader):
        return Dataset.load_from_df(data, reader)

    # For Train_test_validation

    def fit(self):
        self.algo.fit(self.train)

    def predict(self):
        allPred = self.algo.test(self.test, verbose=False)
        pred = []
        for x in allPred:
           pred.append(x.est)
        return pred

    # For K-Fold Cross Validation

    def cross_val_fit(self, train):
        self.algo.fit(train)

    def cross_val_predict(self, test_set):
        allPred = self.algo.test(test_set, verbose=False)
        return allPred

    def cross_val_rmse(self, KFoldNum):
        cross_val = cross_validate(self.algo, self.data, measures=['RMSE'], cv=KFoldNum, verbose=False)
        return cross_val['test_rmse']

    def cross_val_recall(self, predictions, k=5, threshold=3):
        '''Return recall at k metrics for each user.'''

        # First map the predictions to each user.
        user_est_true = defaultdict(list)
        for uid, _, true_r, est, _ in predictions:
            user_est_true[uid].append((est, true_r))

        recalls = dict()
        for uid, user_ratings in user_est_true.items():
            # Sort user ratings by estimated value
            user_ratings.sort(key=lambda x: x[0], reverse=True)

            # Number of relevant items
            n_rel = sum((true_r >= threshold) for (_, true_r) in user_ratings)

            # Number of relevant and recommended items in top k
            n_rel_and_rec_k = sum(((true_r >= threshold) and (est >= threshold))
                                  for (est, true_r) in user_ratings[:k])

            # Recall@K: Proportion of relevant items that are recommended
            recalls[uid] = n_rel_and_rec_k / n_rel if n_rel != 0 else 1

        return recalls



