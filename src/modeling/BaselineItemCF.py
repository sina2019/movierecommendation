import pandas as pd
import math
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import scale

import sys

sys.path.append('../../')

from src.preparation.utility import normalise_movie_movie_data


class BaselineItemCFModel:
    def __init__(self):
        self.similarity = []
        # Attribute to store the training data, in our 
        # case we want a normalised dataset to be reusable
        self.train_data = pd.Series({})
        self.test_data = pd.Series({})
        self.averages_by_id = pd.Series([])

    def fit(self, training):
        normalised_training = normalise_movie_movie_data(training)

        self.train_data = normalised_training

        self.averages_by_id = (
            self.train_data.groupby('movie_id')['rating']
                .mean()
                .rename('average_rating')
        )

        # Pivot the table into movie as row, user as column, rating as the values that each movie row has from the user.
        normalised_user_movie_rating = normalised_training.pivot_table(index='movie_id', columns='user_id',
                                                                       values='rating').fillna(0)

        # Get the cosine similarity (movie-movie).
        normalised_movie_similarity = pd.DataFrame(cosine_similarity(
            scale(normalised_user_movie_rating.fillna(0))),
            index=normalised_user_movie_rating.index,
            columns=normalised_user_movie_rating.index
        )

        self.similarity = normalised_movie_similarity

        return self

    @staticmethod
    def get_top_n_similar_movies(similarity_matrix, training_set, target_user, target_movie, n=3):
        # only get movies where a certain user has rated. i.e. all movies that user id 5 has rated.
        # This is like how lecture 5 works.
        rating_from_user = training_set[training_set['user_id'] == target_user]

        # get all similarity value from this movie to all other
        # movies that has been rated by the same user in the train set.
        similar_users = similarity_matrix[target_movie].filter(items=rating_from_user['movie'].values)

        # Get the top n users.
        # get start from 1 because the first index "0" is always the target_movie itself.
        top_n_similar_users = similar_users.sort_values(ascending=False).iloc[1:(1 + n)]

        return top_n_similar_users

    def predict(self, test):
        normalised_test = normalise_movie_movie_data(test)
        self.test_data = normalised_test

        true_rating = self.train_data['rating']

        # Average rating used if no similar movies at all.
        # TODO: Can use the average rating for the movie instead?
        average_rating = round(true_rating.mean(), 3)
        predicted_rating = [0] * len(self.test_data['rating'])

        # iterate through all the ratings data in the test set.
        for index, row in normalised_test.iterrows():
            this_movie_id = row['movie_id']
            this_user_id = row['user_id']
            try:
                cumulative_rating = 0
                cumulative_similarity = 0
                # Similarity value comes from the training set. (self.similarity)
                similar_movies = self.get_top_n_similar_movies(
                    self.similarity, self.train_data, this_user_id, this_movie_id)

                # Iterate through all similar movies
                for movie, similarity_value in similar_movies.iteritems():
                    user = self.train_data['user_id'] == this_user_id
                    movie = self.train_data['movie_id'] == movie
                    # Get the specific rating (row) that a user give to the similar movie
                    to_predict = self.train_data[user & movie]
                    cumulative_rating += round(
                        to_predict['rating'].values[0] * similarity_value, 3)
                    cumulative_similarity += round(similarity_value, 3)
                if cumulative_rating != 0 and cumulative_similarity != 0:
                    predicted_rating[index] = round(cumulative_rating / cumulative_similarity, 3)

                if math.isnan(predicted_rating[index]) or np.isinf(predicted_rating[index]):
                    try:
                        predicted_rating[index] = self.averages_by_id[this_movie_id]
                    except KeyError:
                        predicted_rating[index] = average_rating
            except KeyError:
                try:
                    predicted_rating[index] = self.averages_by_id[this_movie_id]
                except KeyError:
                    predicted_rating[index] = average_rating

        return predicted_rating

# TESTING PURPOSE

# import sys
# sys.path.append('../../')
#
# from src.preparation.data import Data
# from src.modeling.Models import Models
#
# datasets = Data.get_rating(filtered=True)

# models = {}
# models.update({'Item CF': BaselineItemCFModel()})
#
# models = Models(models)
#
# models.evaluate(datasets)
#
#
# from math import sqrt
# from sklearn.metrics import mean_squared_error
#
# training_a, testing_a = Data.get_datasets_a()
#
# data = BaselineItemCFModel().fit(training_a).predict(testing_a)
# print(data)
#
# print(sqrt(mean_squared_error(normalise_movie_movie_data(testing_a)['rating'], data)))
