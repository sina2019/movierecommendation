import numpy as np


class DampedBaselineModel:
    """
    Baseline modeling that of the form mu + b_u + b_i,
    where mu is the overall average, b_u is a damped user
    average rating residual, and b_i is a damped item (movie)
    average rating residual. See eqn 2.1 of
    http://files.grouplens.org/papers/FnT%20CF%20Recsys%20Survey.pdf
    (Ben Lindsay 2018)
    """

    def __init__(self, damping_factor=0):
        self.damping_factor = damping_factor

    def fit(self, training):
        """Fit training data.

        Parameters
        ----------
        training : DataFrame, shape = [n_samples, >=3]
            User, movie, rating dataFrame. Columns beyond 3 are ignored

        Returns
        -------
        self : object
        """
        # Take the user id, movie id and rating column.
        training = training.iloc[:, :3].copy()
        training.columns = ['user', 'item', 'rating']
        # Mean of the training set ratings.
        self.mu = np.mean(training['rating'])
        user_counts = training['user'].value_counts()
        movie_counts = training['item'].value_counts()

        b_u = (
            # b_u=  1/(|I_u| + β_u) ∑_(iϵI_u)->(r_(u,i) - μ) 
            training[['user', 'rating']]
            .groupby('user')['rating']
            .sum()
            .subtract(user_counts * self.mu)
            .divide(user_counts + self.damping_factor)
            .rename('b_u')
        )
        training = training.join(b_u, on='user')

        # Finding movie residuals.
        training['item_residual'] = training['rating'] - training['b_u'] - self.mu

        b_i = (
            # b_i = 1/(|U_i|+ β_i) ∑_(iϵU_i)->(r_(u,i)- b_u - μ) 
            training[['item', 'item_residual']]
            .groupby('item')['item_residual']
            .sum()
            .divide(movie_counts + self.damping_factor)
            .rename('b_i')
        )
        self.b_u = b_u
        self.b_i = b_i
        return self

    def predict(self, test):
        test = test.iloc[:, :2].copy()
        test.columns = ['user', 'item']
        test = test.join(self.b_u, on='user').fillna(0)
        test = test.join(self.b_i, on='item').fillna(0)
        prediction = self.mu + test['b_u'] + test['b_i']

        return prediction.values

