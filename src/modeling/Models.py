# from src.modeling import (
#     AverageByIdModel,
#     AverageModel,
#     BaselineItemCF,
#     BaselineUserCF,

# )

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import time
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error
from src.preparation.utility import (
    normalise_movie_movie_data,
    normalise_user_user_data
)
from math import sqrt


class Models:
    def __init__(self, models):
        self.models = models

    # Function to evaluate Root Mean Squared Error and Recall.
    def evaluate(self, datasets, n_splits=5, random_state=None, rating_col='rating'):
        kf = KFold(n_splits=n_splits, random_state=random_state, shuffle=True)
        rmses = {}
        recalls = {}
        for model_name, model in self.models.items():
            print(model_name)
            rmses[model_name] = []
            recalls[model_name] = []
            i = 0
            for train_split, test_split in kf.split(datasets):
                start = time.time()
                train_dataset, test_dataset = datasets.iloc[train_split], datasets.iloc[test_split]
                pred = model.fit(train_dataset).predict(test_dataset)
                if model_name == 'Item CF':
                    test_dataset = normalise_movie_movie_data(test_dataset)
                if model_name == 'User CF':
                    test_dataset = normalise_user_user_data(test_dataset)
                rmse = sqrt(mean_squared_error(test_dataset[rating_col], pred))
                rmses[model_name].append(rmse)
                recall = Models.get_all_recommendations(test_dataset, pred)
                recalls[model_name].append(recall)
                end = time.time()
                print("KFold Iteration", i, ": Recall:", round(recall, 2), " RMSE:", round(rmse, 2), "Time Taken:", round(end-start, 2))
                i += 1

        return rmses, recalls

    @staticmethod
    def get_recommendation(target_user, test_data, predicted_data, n=5):
        # need to copy to avoid warning.
        copy_test_data = test_data.copy()
        copy_test_data = copy_test_data.assign(predicted_rating=predicted_data)
        target_rating = copy_test_data['user_id'] == target_user

        to_recommend = copy_test_data[target_rating]

        recommended = to_recommend.sort_values(by='predicted_rating', ascending=False).head(n)
        top_true = to_recommend.sort_values(by='rating', ascending=False).head(n)

        recall = len(recommended[recommended.apply(tuple, 1).isin(top_true.apply(tuple, 1))]) / n

        # """ Other Way to get Recall """ ####
        # Get all the total relevant movies from the movies that this user give rating to in the test set
        # total_relevant = 0
        #
        # for index, row in to_recommend.iterrows():
        #     if row['rating'] >= 3:
        #         total_relevant += 1
        #
        # # Used if we need to find precision as well.
        # total_recommended = len(recommended.index)
        # recommended_relevant = 0
        #
        # for index, row in recommended.iterrows():
        #     if row['rating'] >= 3:
        #         recommended_relevant += 1
        #
        # # If no relevant movies for this user.
        # try:
        #     recall = recommended_relevant / total_relevant
        # except ZeroDivisionError:
        #     print('There is no relevant movies for user: ', target_user, ' in the dataset, recall value -> 1')
        #     return 1

        return recall

    # Function to get all recommendations and recall values
    @staticmethod
    def get_all_recommendations(test_data, predicted_data):
        recall = []

        for user in test_data.user_id.unique():
            recall.append(Models.get_recommendation(user, test_data, predicted_data))

        return np.mean(recall)

    @staticmethod
    def plot(rmses, recalls):
        df_rmses = pd.DataFrame(rmses)
        df_recalls = pd.DataFrame(recalls)

        df_rmses = pd.melt(df_rmses, value_vars=df_rmses.columns).rename({'variable': 'Baseline Model', 'value': 'RMSE'}, axis=1)
        df_recalls = pd.melt(df_recalls, value_vars=df_recalls.columns).rename({'variable': 'Baseline Model', 'value': 'Recall'}, axis=1)
        
        fig, (ax0, ax1) = plt.subplots(2, 1, figsize=(12,8))
        sns.swarmplot(data=df_rmses, x='Baseline Model', y='RMSE', ax=ax1)
        sns.swarmplot(data=df_recalls, x='Baseline Model', y='Recall', ax=ax0)
        ax0.xaxis.set_visible(False)
        plt.tight_layout()
        plt.show()

    def evaluate_no_split(self, training, test, rating_col='rating'):
        rmses = {}
        recalls = {}
        for model_name, model in self.models.items():
            start = time.time()
            print(model_name)
            pred = model.fit(training).predict(test)
            if model_name == 'Item CF':
                test = normalise_movie_movie_data(test)
            if model_name == 'User CF':
                test = normalise_user_user_data(test)
            rmse = sqrt(mean_squared_error(test[rating_col], pred))
            rmses[model_name] = rmse
            recall = Models.get_all_recommendations(test, pred)
            recalls[model_name] = recall
            end = time.time()
            print("RMSE :", round(rmse, 2), "Recall :", round(recall, 2), " Time Taken :", round(end - start, 2))

        return rmses, recalls

    @staticmethod
    def plot_no_split(rmse, recall):
        df_rmse = pd.DataFrame([rmse])
        df_recall = pd.DataFrame([recall])

        df_rmse = pd.melt(df_rmse, value_vars=df_rmse.columns).rename({'variable': 'Baseline Model', 'value': 'RMSE'}, axis=1)
        df_recall = pd.melt(df_recall, value_vars=df_recall.columns).rename({'variable': 'Baseline Model', 'value': 'Recall'}, axis=1)

        fig, (ax0, ax1) = plt.subplots(2, 1, figsize=(12,8))

        sns.barplot(data=df_rmse, x='Baseline Model', y='RMSE', ax=ax0)
        sns.barplot(data=df_recall, x='Baseline Model', y='Recall', ax=ax1)
        ax0.xaxis.set_visible(False)
        plt.tight_layout()
        plt.show()


