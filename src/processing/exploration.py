from src.preparation.data import Data

ratings = Data.get_rating()
users = Data.get_user()
movies = Data.get_movie()
data_a = get_datasets_a()

movie_ratings = ratings.drop(["user_id", "timestamp"], axis=1)
movie_rating_mean = movie_ratings.groupby(['movie_id']).mean()
movie_rating_count = movie_ratings.groupby(['movie_id']).count()

user_ratings = ratings.drop(["movie_id", "timestamp"], axis=1)
user_ratings_count = user_ratings.groupby(['user_id']).count()
user_ratings_mean = user_ratings.groupby(['user_id']).mean()

movie_rating_count.describe()  # number of ratings each movie has got
movie_rating_count[movie_rating_count["rating"] < 5]
movie_rating_count.sort_values(by='rating', ascending=False).head(10)

movie_rating_mean.describe()  # mean rating of each movie

user_ratings_count.sort_values(by='rating', ascending=False).head(10)
user_ratings_count.describe()  # number of ratings given by each user

user_ratings_mean.describe()  # average rating given by each user
user_ratings_mean[user_ratings_mean["rating"] < 2 ]
user_ratings_mean[user_ratings_mean["rating"] > 4 ].sort_values(by='rating', ascending=False).head(10)

user405 = ratings[ratings["user_id"] == 405]

temp_merge = ratings.merge(movie_rating_count, on='movie_id', how='left')
temp_merge[temp_merge.rating_y > 5].drop(["rating_y"], axis=1)


export_csv = temp_merge[temp_merge.rating_y > 5].drop(["rating_y"], axis=1).to_csv(Data.path_100k + "/Filt_Data.data",

temp_merge = movies.merge(movie_rating_count, on='movie_id', how='left')
movies_w_ratings = temp_merge.sort_values(by='rating', ascending=False).head(30).drop(["video_release_date", "IMDb URL"], axis=1)


export_csv = temp_merge[temp_merge.rating_y > 5].drop(["rating_y"], axis=1).to_csv((Data.processed_path_100k + "/Filt_Data.data", index=None, header=False)

export_csv = movie_rating_count.to_csv(Data.processed_path_100k + "/movie_ratings_count.csv", index=None, header=False)

export_csv = user_ratings_count.to_csv(Data.processed_path_100k + "/user_ratings_count.csv", index=None, header=False)


ratingsF = Data.get_rating(filtered=True)
user_ratingsF = ratingsF.drop(["movie_id", "timestamp"], axis=1)
user_ratingsF_count = user_ratingsF.groupby(['user_id']).count()
#user_cut = user_ratingsF_count[user_ratingsF_count["rating"] < 319]
#temp_merge = user_cut.merge(ratingsF, on='user_id', how='left')
temp_merge = ratingsF.merge(user_ratingsF_count, on='user_id', how='left')
users_cut = temp_merge[temp_merge["rating_y"] < 319].drop(["rating_y"], axis=1)
export_csv = users_cut.to_csv(Data.processed_path_100k + "/u.data", index=None, header=False)
