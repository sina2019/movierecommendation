import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from src.preparation.data import Data
from collections import OrderedDict
from src.preparation.utility import get_err_and_res
from src.modeling.AverageModel import AverageModel
from src.modeling.AverageByIdModel import AverageByIdModel
from src.modeling.DampedBaselineModel import DampedBaselineModel

plt.style.use('big-darkgrid.mplstyle')

ratings = Data.get_rating()
users = Data.get_user()
movies = Data.get_movie()

# Plotting User Age Distribution
users.age.plot.hist(bins=10)
plt.title("Distribution of users' ages")
plt.ylabel('count of users')
plt.xlabel('age')

# Plotting Ratings Distribution
norm_counts = (
    ratings['rating']
        .value_counts(normalize=True, sort=False)
        .multiply(100)
        .reset_index()
        .rename(columns={'rating': 'percent', 'index': 'rating'})
)
ax = sns.barplot(x='rating', y='percent', data=norm_counts)
ax.set_title('Rating Frequencies')
plt.show()

# ERRORS AND RESIDUAL PLOTTING.
err1, res1 = get_err_and_res(ratings, AverageModel())
err2, res2 = get_err_and_res(ratings, AverageByIdModel('movie_id'))
err3, res3 = get_err_and_res(ratings, AverageByIdModel('user_id'))
err4, res4 = get_err_and_res(ratings, DampedBaselineModel(0))
err5, res5 = get_err_and_res(ratings, DampedBaselineModel(10))
err6, res6 = get_err_and_res(ratings, DampedBaselineModel(25))
err7, res7 = get_err_and_res(ratings, DampedBaselineModel(50))

# Collate errors data into an OrderedDict => (('Average', err1[0]), ('Average', err2[0]),
#  ..... ('Combined 50', err7[4]));
errs_data = OrderedDict(
    (
        ('Average', err1),
        ('Item Average', err2),
        ('User Average', err3),
        ('Combined 0', err4),
        ('Combined 10', err5),
        ('Combined 25', err6),
        ('Combined 50', err7),
    )
)

# Convert into pandas dataframe.
df_errs = pd.DataFrame(errs_data)

# display(df_errs)

# convert (collate) all columns into one row.
df_errs = (
    pd.melt(df_errs, value_vars=df_errs.columns)
        .rename({'variable': 'Baseline modeling', 'value': 'MSE'}, axis=1)
)

res_data = OrderedDict(
    (
        ('Average', res1),
        ('Item Average', res2),
        ('User Average', res3),
        ('Combined 0', res4),
        ('Combined 10', res5),
        ('Combined 25', res6),
        ('Combined 50', res7),
    )
)

df_res = pd.DataFrame(res_data)

# display(df_res.tail())
df_res = (
    pd.melt(df_res, value_vars=df_res.columns)
        .rename({'variable': 'Baseline modeling', 'value': 'Residual'}, axis=1)
)

fig, (ax0, ax1) = plt.subplots(2, 1, figsize=(12, 8))
sns.swarmplot(data=df_errs, x='Baseline modeling', y='MSE', ax=ax0)
sns.violinplot(data=df_res, x='Baseline modeling', y='Residual', ax=ax1)
ax0.xaxis.set_visible(False)
plt.tight_layout()
plt.show()

# ERRORS AND RESIDUAL PLOTTING ENDS HERE #################
