import pandas as pd
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_log_error, mean_absolute_error, confusion_matrix
import numpy as np


def read_csv(path, column_header, separator="\t"):
    return pd.read_csv(path, sep=separator, engine='python', names=column_header)


def get_err_and_res(datasets, model, n_splits=5, random_state=0, rating_col='rating'):
    kf = KFold(n_splits=n_splits, random_state=random_state, shuffle=True)
    mses = []
    residuals = np.zeros(len(datasets))
    for train_index, test_index in kf.split(datasets):
        train_dataset, test_dataset = datasets.iloc[train_index], datasets.iloc[test_index]
        pred = model.fit(train_dataset).predict(test_dataset)
        residuals[test_index] = pred - test_dataset[rating_col]
        # Get the mean squared error.
        mse = mean_squared_log_error(test_dataset[rating_col], pred)
        y_test = np.round(test_dataset[rating_col].values)
        # mae = mean_absolute_error(test_dataset[rating_col], pred)
        mses.append(mse)
    return mses, residuals


def normalise_user_user_data(dataset):
    # get the mean of all rating for each different user id.
    mean = dataset.groupby(['user_id'], as_index=False, sort=False).mean().rename(
        columns={"user_id": "user_id", "rating": "rating_mean"})
    # combine the mean to the training.
    dataset = pd.merge(dataset, mean, on='user_id',
                       how="left", sort="False")
    dataset['rating_adjusted'] = round(
        dataset['rating'] - dataset['rating_mean'], 2)

    dataset = pd.DataFrame({"user_id": dataset['user_id'],
                            "movie_id": dataset['movie_id_x'],
                            "rating": dataset['rating_adjusted']
                            })

    return dataset


def normalise_movie_movie_data(dataset):
    mean = dataset.groupby(['movie_id'], as_index=False, sort=False).mean().rename(
        columns={"movie_id": "movie_id", "rating": "rating_mean"})
    # combine the mean to the dataset.
    dataset = pd.merge(dataset, mean, on='movie_id',
                       how="left", sort="False")
    dataset['rating_normalised'] = dataset['rating'] - \
        dataset['rating_mean']

    dataset = pd.DataFrame({"user_id": dataset['user_id_x'],
                            "movie_id": dataset['movie_id'],
                            "rating": dataset['rating_normalised']
                            })

    return dataset


def get_rating_matrix(X):
    """Function to generate a ratings matrx and mappings for
    the user and item ids to the row and column indices

    Parameters
    ----------
    X : pandas.DataFrame, shape=(n_ratings,>=3)
        First 3 columns must be in order of user, item, rating.

    Returns
    -------
    rating_matrix : 2d numpy array, shape=(n_users, n_items)
    user_map : pandas Series, shape=(n_users,)
        Mapping from the original user id to an integer in the range [0,n_users)
    item_map : pandas Series, shape=(n_items,)
        Mapping from the original item id to an integer in the range [0,n_items)
    """
    user_col, item_col, rating_col = X.columns[:3]
    rating = X[rating_col]
    user_map = pd.Series(
        index=np.unique(X[user_col]),
        data=np.arange(X[user_col].nunique()),
        name='user_map',
    )
    item_map = pd.Series(
        index=np.unique(X[item_col]),
        data=np.arange(X[item_col].nunique()),
        name='columns_map',
    )
    user_inds = X[user_col].map(user_map)
    item_inds = X[item_col].map(item_map)
    rating_matrix = (
        pd.pivot_table(
            data=X,
            values=rating_col,
            index=user_inds,
            columns=item_inds,
        )
        .fillna(0)
        .values
    )
    return rating_matrix, user_map, item_map
