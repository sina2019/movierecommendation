from src.preparation.utility import read_csv
from definitions import (
    RAW_DATA_DIR_100k,
    PROCESSED_DATA_DIR_100k,
    RAW_DATA_DIR_20m,
    PROCESSED_DATA_DIR_20m
)


class Data:
    path_100k = RAW_DATA_DIR_100k
    processed_path_100k = PROCESSED_DATA_DIR_100k
    path_20m = RAW_DATA_DIR_20m
    processed_path_20m = PROCESSED_DATA_DIR_20m

    @staticmethod
    def get_rating(filtered=False, bigger=False):
        path = Data.path_100k if not filtered else Data.processed_path_100k
        path_if_bigger = Data.path_20m if not filtered else Data.processed_path_20m
        
        separator = '\t' if not filtered else ','

        r_cols = ['user_id', 'movie_id', 'rating', 'timestamp']

        if not bigger:
            ratings = read_csv(path + "/u.data", r_cols, separator)
        
        if bigger:
            ratings = read_csv(path_if_bigger + "/ratings.csv", r_cols, ',')
        
        return ratings

    @staticmethod
    def get_user():
        # User Information Table
        u_cols = ['user_id', 'age', 'sex', 'occupation', 'zip_code']
        users = read_csv(Data.path_100k + "/u.user", u_cols, '|')

        return users

    @staticmethod
    def get_datasets_a(filtered=False):
        r_cols = ['user_id', 'movie_id', 'rating', 'timestamp']
        path = Data.path_100k if not filtered else Data.processed_path_100k
        separator = '\t' if not filtered else ','

        train_a = read_csv(path + "/ua.base", r_cols, separator)
        test_a = read_csv(path + "/ua.test", r_cols, separator)

        return train_a, test_a

    @staticmethod
    def get_datasets_b():
        r_cols = ['user_id', 'movie_id', 'rating', 'timestamp']
        train_b = read_csv(Data.path_100k + "/ub.base", r_cols, '\t')
        test_b = read_csv(Data.path_100k + "/ub.test", r_cols, '\t')

        return train_b, test_b

    @staticmethod
    def get_movie():
        # Movie Information Table
        m_cols = ['movie_id', 'movie_title', 'release_date', 'video_release_date', 'IMDb URL']
        genres = read_csv(Data.path_100k + "/u.genre", ['genre', 'index'], '|')
        genre_cols = list(genres['genre'])

        m_cols.extend(genre_cols)

        movies = read_csv(Data.path_100k + "/u.item", m_cols, '|')

        return movies

