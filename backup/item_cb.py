import pandas as pd
import warnings
from src.preparation.data import Data
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import scale
from sklearn.metrics import mean_squared_error
from math import sqrt

train_a, test_a = Data.get_datasets_a()


def rating_similarity(dataset):
    user_movie_rating = pd.DataFrame({'user': dataset['user_id'],
                                      'movie': dataset['movie_id'],
                                      'rating': dataset['rating']})
    user_movie_rating = user_movie_rating.pivot_table(index='user', columns='movie', values='rating').fillna(0)

    # define user-user similarity (without any filter at all.)
    similarity = pd.DataFrame(cosine_similarity(
        scale(user_movie_rating.fillna(0))),
        index=user_movie_rating.index,
        columns=user_movie_rating.index
    )

    return similarity


def centered_cosine_rating_similarity(dataset):
    mean = dataset.groupby(['user_id'], as_index=False, sort=False).mean().rename(
        columns={"user_id": "user_id", "rating": "rating_mean"})
    dataset = pd.merge(dataset, mean, on='user_id', how="left", sort="False")
    dataset['rating_adjusted'] = dataset['rating'] - dataset['rating_mean']

    centered_user_movie_rating = pd.DataFrame({"user": dataset['user_id'],
                                               "movie": dataset['movie_id_x'],
                                               "rating": dataset['rating_adjusted']
                                               })
    centered_user_movie_rating = centered_user_movie_rating.pivot_table(index='user', columns='movie',
                                                                        values='rating').fillna(0)

    return centered_user_movie_rating


# similarity = rating_similarity(train_a)
similarity = centered_cosine_rating_similarity(train_a)


def get_top_N_similar_users(similarity_matrix, target_user, n=3):
    # get start from 1 because the first index "0" is always the target_user itself.
    return similarity_matrix[target_user].sort_values(ascending=False).iloc[1:(1 + n)]


def predict_movie(dataset, similarity_matrix, target_user, movie):
    similar_users = get_top_N_similar_users(similarity_matrix, target_user)
    predicted_rating = 0
    for user, similarity_value in similar_users.iteritems():
        predicted_rating += (dataset[movie][user] * similarity_value)
    return predicted_rating / similar_users.count()


def predict_test_movies(similarity_matrix, test_data):
    user_movie_rating = pd.DataFrame({'user': test_data['user_id'],
                                      'movie': test_data['movie_id'],
                                      'rating': test_data['rating']})
    true_rating = user_movie_rating['rating']
    predicted_rating = [0] * len(true_rating)

    for index, row in user_movie_rating.iterrows():
        this_user_id = row['user']
        this_movie_id = row['movie']

        similar_users = get_top_N_similar_users(similarity_matrix, this_user_id)
        for user, similarity_value in similar_users.iteritems():
            user = user_movie_rating['user'] == this_user_id
            movie = user_movie_rating['movie'] == this_movie_id
            to_predict = user_movie_rating[user & movie]
            predicted_rating[index] += (to_predict['rating'].values[0] * similarity_value)
            predicted_rating[index] /= similar_users.count()

    mse = mean_squared_error(true_rating, predicted_rating)
    rmse = sqrt(mse)
    return rmse


print(predict_test_movies(similarity, test_a))
